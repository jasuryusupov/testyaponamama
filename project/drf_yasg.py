from rest_framework_swagger.views import get_swagger_view
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from django.contrib import admin


API_TITLE = 'YaponaMama Project Docs"'
API_DESCRIPTION = 'YaponaMama Project Docs'
yasg_schema_view = get_schema_view(
    openapi.Info(
        title=API_TITLE,
        default_version='v1',
        description=API_DESCRIPTION,
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="burkhonovshakhzod2000@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
)

schema_view = get_swagger_view(title=API_TITLE)


admin.site.site_header = "YaponaMama administration"
admin.site.site_title = "YaponaMama administration"
admin.site.index_title = "YaponaMama Project"
