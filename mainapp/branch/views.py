from rest_framework.viewsets import mixins, GenericViewSet
from drf_yasg.utils import swagger_auto_schema
from mainapp.branch import filter_params
from rest_framework.permissions import IsAuthenticated
from mainapp.branch import models
from mainapp.branch import serializers
from rest_framework.views import APIView, Response

class BranchInfoView(mixins.ListModelMixin,
                     GenericViewSet):
    queryset = models.Branch.objects.all()
    serializer_class = serializers.BranchInfoSerializer

    # permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        region = self.request.query_params.get("region")
        if region:
            queryset = models.Branch.objects.filter(region__region=region)
        return queryset

    @swagger_auto_schema(manual_parameters=filter_params.get_region_name())
    def list(self, request, *args, **kwargs):
        return super(BranchInfoView, self).list(kwargs)
