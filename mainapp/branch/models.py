from django.db import models
from mainapp.account.models import Region


class Manager(models.Model):
    first_name = models.CharField(max_length=50, blank=True, null=True)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    phone_number = models.CharField(max_length=13)
    email = models.EmailField(blank=True, null=True)

    def full_name(self):
        return self.first_name + self.last_name


class Branch(models.Model):
    name = models.CharField(max_length=100, blank=True,  null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    region = models.ForeignKey(to=Region, on_delete=models.CASCADE, related_name='branch_region')
    manager = models.ForeignKey(to=Manager, on_delete=models.CASCADE, related_name='branch_manager')
    rating = models.FloatField()