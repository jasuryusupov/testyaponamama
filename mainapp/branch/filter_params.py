from drf_yasg import openapi


def get_branch_id():
    branch_id = openapi.Parameter('branch_id', openapi.IN_QUERY, description='Branch id',
                                    type=openapi.TYPE_NUMBER)
    return [branch_id]


def get_region_name():
    region = openapi.Parameter('region', openapi.IN_QUERY, description='Enter region name for filter',
                               type=openapi.TYPE_STRING)
    return [region]