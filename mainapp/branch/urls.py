from django.urls import path
from mainapp.branch import views
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register("", views.BranchInfoView)

urlpatterns = [
] + router.urls