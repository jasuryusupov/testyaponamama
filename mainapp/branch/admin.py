from django.contrib import admin
from mainapp.branch.models import Branch, Manager


@admin.register(Branch)
class BranchAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'region', 'rating']
    list_display_links = ['id', 'name', 'region', 'rating']


@admin.register(Manager)
class ManagersAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name', 'phone_number']
    list_display_links = ['id', 'first_name', 'last_name']
