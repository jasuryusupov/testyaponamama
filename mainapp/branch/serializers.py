from rest_framework import serializers
from mainapp.branch import models
from mainapp.branch.modules import calculate_star


class ManagersSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Manager
        fields = "__all__"


class BranchInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Branch
        fields = "__all__"

    def to_representation(self, instance):
        data = super(BranchInfoSerializer, self).to_representation(instance)
        data["region"] = instance.region.region
        data["manager"] = ManagersSerializer(instance=instance.manager).data
        data["phone_number"] = instance.manager.phone_number
        sum_of_stars = int(sum(instance.feedback_branch.filter(star__in=[1, 2, 3, 4]).values_list('star', flat=True)))
        num_of_stars = instance.feedback_branch.filter(star__in=[1, 2, 3, 4, 5]).values_list('star', flat=True).count()
        if sum_of_stars and num_of_stars > 0:
            data['rating'] = calculate_star(a=sum_of_stars, b=num_of_stars)
        else:
            data['rating'] = 0
        return data