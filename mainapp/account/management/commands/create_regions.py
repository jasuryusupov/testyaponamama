from django.core.management.base import BaseCommand
from mainapp.account.management.commands import regions
from mainapp.account.models import Region


class Command(BaseCommand):
    help = 'Creating all regions and districs'

    def handle(self, *args, **options):
        regs = []
        for i in range(len(regions.regions)):
            regs.append(regions.regions[i][1])

        Region.objects.bulk_create([Region(region=reg) for reg in regs])
        print("Done")