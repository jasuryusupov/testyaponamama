from django.urls import path
from mainapp.account import views

urlpatterns = [
    path("login/",  views.LoginView.as_view())
]