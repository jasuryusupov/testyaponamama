from rest_framework import serializers
from mainapp.account import models


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CustomUser
        fields = ("username", "first_name", "last_name", "email", "created_at")
