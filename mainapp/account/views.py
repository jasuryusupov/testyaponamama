from rest_framework.views import APIView, Response, status
from drf_yasg.utils import swagger_auto_schema
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from mainapp.account import serializers
from mainapp.account import queryparams
from mainapp.account import models


class LoginView(APIView):
    """ Login view for all admins  """
    @swagger_auto_schema(manual_parameters=queryparams.get_login_password())
    def post(self, request, *args, **kwargs):
        login = self.request.data.get("login", None) or self.request.query_params.get("login")
        password = self.request.data.get("password", None) or self.request.query_params.get("password")
        if login and password:
            try:
                user = models.CustomUser.objects.get(username=login, password=password)
                refresh = TokenObtainPairSerializer().get_token(user)
                user_data = serializers.UserSerializer(instance=user).data
                data = {"refresh": str(refresh), "access": str(refresh.access_token), "user_info": user_data}
                return Response(data=data, status=status.HTTP_200_OK)
            except:
                return Response(data={"error": "user not found"}, status=status.HTTP_403_FORBIDDEN)
        return Response(data={"error": "username and password required"}, status=status.HTTP_400_BAD_REQUEST)