from django.contrib import admin
from django.utils.text import gettext_lazy as _

from .models import CustomUser, Region


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    fieldsets = (
        (_('Main'), {'fields': ('username', 'password')}),
        (_('Personal info'),
         {'fields': ('first_name', 'last_name')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username', 'password1', 'password2', 'first_name', 'last_name', 'created_at'),
        }),
    )

    list_display = ['id', 'username', 'first_name']
    list_display_links = ['id', 'username', 'first_name']


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ['id', 'region']
    list_display_links = ['id', 'region']