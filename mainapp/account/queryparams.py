from drf_yasg import openapi


def get_login_password():
    login = openapi.Parameter('login', openapi.IN_QUERY, description="login",
                              type=openapi.TYPE_STRING)
    password = openapi.Parameter('password', openapi.IN_QUERY, description="password",
                                 type=openapi.TYPE_STRING)

    return [login, password]
