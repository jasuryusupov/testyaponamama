from django.contrib import admin
from mainapp.feedback.models import Feedback


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_display = ['id', 'branch', 'star', 'date']
    list_display_links = ['id', 'branch', 'star', 'date']
