from django.db import models
from mainapp.branch.models import Branch
from django.core.validators import MaxValueValidator, MinValueValidator


class Feedback(models.Model):
    DELIVERY = "delivery"
    RESTAURANT = "restaurant"
    FEEDBACK_TYPE = (
        (DELIVERY, "delivery"),
        (RESTAURANT, "restaurant")
    )
    branch = models.ForeignKey(to=Branch, on_delete=models.CASCADE, related_name='feedback_branch')
    comment = models.TextField()
    phone_number = models.CharField(max_length=13)
    audio = models.FileField(upload_to='audio/')
    image = models.ImageField(upload_to='image/')
    feedback_type = models.CharField(max_length=10, choices=FEEDBACK_TYPE)
    date = models.DateTimeField(auto_now_add=True)
    star = models.IntegerField(validators=[MaxValueValidator(4), MinValueValidator(1)])
