from drf_yasg import openapi


def get_feedback_type():
    feedback_type = openapi.Parameter('feedback_type', openapi.IN_QUERY, description='Write Feedback type',
                                      type=openapi.TYPE_STRING)
    return [feedback_type]


def get_branch_id():
    branch_id = openapi.Parameter("branch_id", openapi.IN_QUERY, description="Write branch id",
                                  type=openapi.TYPE_NUMBER)
    return [branch_id]