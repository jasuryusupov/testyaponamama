from rest_framework import serializers
from mainapp.feedback import models
from django.shortcuts import get_object_or_404


class FeedbackListSerializer(serializers.ModelSerializer):
    branch = serializers.SerializerMethodField

    class Meta:
        model = models.Feedback
        fields = ("id", "phone_number", "branch", "feedback_type", "date")

    def get_branch(self, instance):
        return instance.branch.name

    def to_representation(self, instance):
        data = super(FeedbackListSerializer, self).to_representation(instance)
        data["branch"] = instance.branch.name
        if instance.audio:
            data["audio"] = True
        if instance.image:
            data["image"] = True
        if not instance.audio:
            data["audio"] = False
        if not instance.image:
            data["image"] = False
        return data


class FeedbackRetrieveSerializer(serializers.ModelSerializer):
    branch = serializers.SerializerMethodField()

    class Meta:
        model = models.Feedback
        fields = ("phone_number", "comment", "branch", "star", "image", "audio")

    def get_branch(self, instance):
        return instance.branch.name


class FeedbackCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Feedback
        fields = ("phone_number", "comment", "star", "feedback_type", "image", "audio")

    def create(self, validated_data):
        branch_id = self.context.get("branch_id")
        data = self.Meta.model.objects.create(branch=get_object_or_404(models.Branch, id=branch_id), **validated_data)
        return data