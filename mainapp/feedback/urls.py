from django.urls import path
from rest_framework.routers import SimpleRouter
from mainapp.feedback import views

router = SimpleRouter()
router.register("", views.FeedbacksListView)

urlpatterns = [
    path("create/", views.FeedbackCreateView.as_view({"post": "create"})),
    path("TestApi2", views.TestApi.as_view())
] + router.urls