from rest_framework.viewsets import mixins, GenericViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from django.shortcuts import get_object_or_404
from rest_framework.views import Response, status
from drf_yasg.utils import swagger_auto_schema
from mainapp.feedback import filter_params
from mainapp.feedback import models
from mainapp.feedback import serializers
from rest_framework.views import APIView, Response


class FeedbacksListView(mixins.ListModelMixin,
                        mixins.RetrieveModelMixin,
                        GenericViewSet):
    queryset = models.Feedback.objects.all()
    serializer_class = serializers.FeedbackListSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = PageNumberPagination

    def retrieve(self, request, *args, **kwargs):
        data = get_object_or_404(models.Feedback, id=self.kwargs.get("pk"))
        data = serializers.FeedbackRetrieveSerializer(instance=data, context={"request": self.request}).data
        return Response(data=data, status=status.HTTP_200_OK)

    def get_queryset(self):
        feedback_type = self.request.query_params.get("feedback_type")
        if feedback_type == models.Feedback.DELIVERY:
            self.queryset = self.queryset.filter(feedback_type=models.Feedback.DELIVERY)
            return self.queryset
        if feedback_type == models.Feedback.RESTAURANT:
            self.queryset = self.queryset.filter(feedback_type=models.Feedback.RESTAURANT)
            return self.queryset
        return []

    @swagger_auto_schema(manual_parameters=filter_params.get_feedback_type())
    def list(self, request, *args, **kwargs):
        return super(FeedbacksListView, self).list(kwargs)


class FeedbackCreateView(mixins.CreateModelMixin,
                         GenericViewSet):
    queryset = models.Feedback.objects.all()
    serializer_class = serializers.FeedbackCreateSerializer
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(manual_parameters=filter_params.get_branch_id())
    def get_serializer_context(self):
        branch_id = self.request.query_params.get("branch_id")
        return {"request": self.request, "branch_id": branch_id}


class TestApi2(APIView):
    def get(self, request):
        return Response({"success": "Done"})